import 'webpack-dev-server';

import path from "node:path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

const config = {
    devtool: "source-map",
    mode: "development",
    resolve: {
        extensions: ['.ts', '.js'],
      extensionAlias: {'.js': ['.js', '.ts']}
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve('src/html/index.html'),
        }),
        new MiniCssExtractPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.s?css$/i,
                // use: ["style-loader", "css-loader","sass-loader"],
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    /* Je kan postCSS gebruiken om met autoprefixer vendor prefixes toe te voegen aan je gegeneerde css
                    * https://github.com/postcss/autoprefixer
                    */
                    // {
                    //     loader: 'postcss-loader',
                    //     options: {
                    //         postcssOptions: {
                    //             plugins: () => [
                    //                 require('autoprefixer')
                    //             ]
                    //         }
                    //     }
                    // },
                    'sass-loader'
                ]
            },
            {test: /\.html?$/i, use: ['html-loader']},
            // Image assets
            {test: /\.(png|svg|jpe?g|gif)$/i, type: "asset"},
            // Font assets
            {test: /\.(woff2?|eot|ttf|otf)$/i, type: "asset"},
        ]
    },
    output: {
        clean: true
    },
    devServer: {
        static: {directory: path.resolve("dist")},
        open: true,
        liveReload: true,
        hot: false
    }
}
export default config
